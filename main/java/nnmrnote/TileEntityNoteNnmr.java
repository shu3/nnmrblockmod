package nnmrnote;

import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityNote;
import net.minecraft.world.World;

public class TileEntityNoteNnmr extends TileEntityNote {
    
    /**
     * plays the stored note
     */
    public void triggerNote(World world, int x, int y, int z)
    {
        if (world.getBlock(x, y + 1, z).getMaterial() == Material.air)
        {
            Material material = world.getBlock(x, y - 1, z).getMaterial();
            byte b0 = 0;

            if (material == Material.rock)
            {
                b0 = 1;
            }

            if (material == Material.sand)
            {
                b0 = 2;
            }

            if (material == Material.glass)
            {
                b0 = 3;
            }

            if (material == Material.wood)
            {
                b0 = 4;
            }

            world.addBlockEvent(x, y, z, NnmrNoteMod.nnmrBlock, b0, this.note);
        }
    }
}
