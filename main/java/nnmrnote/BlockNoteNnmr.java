package nnmrnote;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockNote;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.entity.RenderEntity;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityNote;
import net.minecraft.util.IIcon;
import net.minecraft.world.IWorldAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockNoteNnmr extends BlockNote {
	
	static private IIcon nnmrFace;
	
	public BlockNoteNnmr() {
		super();
		this.setBlockTextureName("nnmrnote:noteblock");
	}
	
	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {
		super.registerBlockIcons(iconRegister);
		this.nnmrFace = iconRegister.registerIcon("nnmrnote:nnmrface");
	}

    /**
     * Returns a new instance of a block's tile entity class. Called on placing the block.
     */
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_)
    {
        return new TileEntityNoteNnmr();
    }

    @SideOnly(Side.CLIENT)
    public boolean onBlockEventReceived(World world, int x, int y, int z, int type, int pitch)
    {
        float f = (float)Math.pow(2.0D, (double)(pitch-12) / 12.0D);
        String s = "harp";
        
        // typeによって s を変える
        if (type == 1)
        {
            s = "bd";
        }

        if (type == 2)
        {
            s = "snare";
        }

        if (type == 3)
        {
            s = "hat";
        }

        if (type == 4)
        {
            s = "bassattack";
        }
        
        world.playSoundEffect((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, "nnmrnote:" + s, 3.0F, f);
        spawnFace(world, x + 0.5D, y + 1.2D, z + 0.5D, pitch);
        return true;
    }

	private void spawnFace(World world, double x, double y, double z, int pitch) {
		Minecraft mc = net.minecraft.client.Minecraft.getMinecraft();
		// 鷹さんの顔を出す。音の高さによって飛ぶ高さがことなる
		if (mc != null && mc.renderViewEntity != null && mc.effectRenderer != null)
        {
			// renderの範囲でなければ弾く（サボるとたまにNullPointerExceptionがでる）
            double d6 = mc.renderViewEntity.posX - x;
            double d7 = mc.renderViewEntity.posY - y;
            double d8 = mc.renderViewEntity.posZ - z;
            double d9 = 16.0D;
            if (d6 * d6 + d7 * d7 + d8 * d8 > d9 * d9)
            {
                return;
            }

			EntityNnmrFaceFx fx = new EntityNnmrFaceFx(world, (double)x, (double)y, (double)z, 0, pitch, 0, this.nnmrFace);
			mc.effectRenderer.addEffect(fx);
        }
	}
}
