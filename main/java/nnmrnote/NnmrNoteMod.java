package nnmrnote;

import net.minecraft.block.Block;
import net.minecraft.block.BlockNote;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = NnmrNoteMod.MODID, version = NnmrNoteMod.VERSION)
public class NnmrNoteMod {
    public static final String MODID = "nnmrnote";
    public static final String VERSION = "1.0";
    public static Block nnmrBlock;

    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
    	this.nnmrBlock = (new BlockNoteNnmr()).setBlockName("blockNoteNnmr").setHardness(0.8F);
    	GameRegistry.registerBlock(nnmrBlock, "blockNoteNnmr");
    	GameRegistry.registerTileEntity(TileEntityNoteNnmr.class, "NnmrMusic");
	}
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		// some example code
    }
}
